1. **Создание пользователя** 

		HTTP-метод: POST  
		URL: /technodomtestapp/user  			
		
			Тело запроса:
			
			{
				"name" : <text>,
               	"email" : <text>,
                "url":<text>
			}
			
			
			Формат ответа:
			{
                "id":<int>
            }
			
2. **Получение информации по пользователю** 

		HTTP-метод: GET  
		URL: /technodomtestapp/user/:id  			

			Формат ответа
			
			{
                "id" : <int>,
				"name" : <text>,
               	"email" : <text>,
                "url":<text>,
                "isOnline":<boolean>
			}
			
3. **Изменение статуса пользователя** 

		HTTP-метод: PUT  
		URL: /technodomtestapp/user/:id  			

			Формат запроса
			
			{
                "isOnline":<boolean>
			}
            
           	Формат ответа:
			{               
				"user":
							{
							"id":<int>,
							"isOnline":<boolean>
							}, 
				"previousState":
							{
							"isOnline":<boolean>
							}
			}
			
4. **Получение списка пользователей** 

		HTTP-метод: GET  
		URL: /technodomtestapp/users
		Параметры: 
               "isOnline" <boolean> 
               "requestId" : <long>

			Формат ответа
			
			{
				"requestId":<long>,
				"users":[
					{
					"url":<text>
					"isOnline":<boolean>
					},...
				]
			}
       