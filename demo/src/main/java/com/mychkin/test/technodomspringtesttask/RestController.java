/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mychkin.test.technodomspringtesttask;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mychkin.test.technodomspringtesttask.entities.Users;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("technodomtestapp")
@Component
public class RestController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> createUser(@RequestBody Users user) {
        Users validatedUser = new Users();

        validatedUser.setName(user.getName());
        validatedUser.setEmail(user.getEmail());
        validatedUser.setUrl(user.getUrl());
        validatedUser.setIsOnline(user.getIsOnline());

        userRepository.save(validatedUser);
        return ResponseEntity.ok(validatedUser.toJson().toString());
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getUser(@PathVariable(value = "id") Integer id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Users user = userRepository.findUserById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(user.toJson().toString());
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getUsers(@RequestParam(value = "isOnline", required = false) Boolean isOnline,
            @RequestParam(value = "requestId", required = false) Long requestId) {
        List<Users> users;
        if (isOnline != null) {
            users = userRepository.findUsersByIsOnline(isOnline);
        } else {
            users = (List<Users>) userRepository.findAll();
        }
        JsonArray usersJsonArray = new JsonArray();
        users.forEach(user -> usersJsonArray.add(user.toJsonIsOnlineUrl()));
        JsonObject response = new JsonObject();
        response.addProperty("requestId", requestId);
        response.add("users", usersJsonArray);
        return ResponseEntity.ok(response.toString());
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> updateUser(@RequestBody Users user, @PathVariable(value = "id") Integer id) {
        if (user == null || id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Users userFromDb = userRepository.findUserById(id);
        if (userFromDb == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        JsonObject previousState = new JsonObject();
        previousState.addProperty("isOnline", userFromDb.getIsOnline());
        
        userFromDb.setIsOnline(user.getIsOnline());
        userRepository.save(userFromDb);

        JsonObject response = new JsonObject();
        response.add("user", userFromDb.toJsonIdIsOnline());
        response.add("previousState", previousState);

        return ResponseEntity.ok(response.toString());
    }

}
