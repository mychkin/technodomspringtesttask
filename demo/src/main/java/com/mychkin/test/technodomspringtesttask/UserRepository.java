/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mychkin.test.technodomspringtesttask;

import com.mychkin.test.technodomspringtesttask.entities.Users;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface UserRepository extends CrudRepository<Users, Integer> {

    public List<Users> findUsersByName(String name);

    public Users findUserById(Integer id);

    public List<Users> findUsersByIsOnline(Boolean name);

}
